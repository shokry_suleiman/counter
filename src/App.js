import "./App.css";
import Navbar from "./components/navbar";
import Counters from "./components/counters";
import React, { Component } from "react";

class App extends Component {
  state = {
    counters: [
      { id: 1, value: 5 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 },
    ],
  };

  constructor() {
    super();
    console.log("app js constructor");
  }

  componentDidMount() {
    console.log(`did mount app js`);
  }
  handleDelete = (counterId) => {
    console.log("Handle Delete Event", counterId);
    const counters = this.state.counters.filter((c) => c.id !== counterId);
    this.setState({ counters });
  };

  handleReset = () => {
    console.log("Handle Reset");
    const counters = this.state.counters.map((c) => {
      c.value = 0;
      return c;
    });
    console.log("counters", counters);
    this.setState({ counters });
    console.log("this.state.counters", this.state.counters);
  };

  handleIncrement = (counter) => {
    console.log("counter from increment--->>", counter);
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };
  render() {
    console.log(`render app`);
    return (
      <React.Fragment>
        <Navbar counters={this.state.counters.filter((c) => c.value).length} />
        <main className="container">
          <Counters
            counters={this.state.counters}
            onDelete={this.handleDelete}
            onReset={this.handleReset}
            onIncrement={this.handleIncrement}
          />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
