import React, { Component } from "react";
import Counter from "./counter";
class Counters extends Component {
  render() {
    console.log(`render counters`);

    const { onReset, counters, onDelete, onIncrement } = this.props;
    return (
      <div>
        <button className="btn btn-primary btn-sm m-2" onClick={onReset}>
          Reset
        </button>
        {counters.map((counter) => (
          <Counter
            counter={counter}
            key={counter.id}
            value={counter.value}
            selected={true}
            onDelete={onDelete}
            id={counter.id}
            onIncrement={onIncrement}
          >
            <h4>Title</h4>
          </Counter>
        ))}
      </div>
    );
  }
}

export default Counters;
