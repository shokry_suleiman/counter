import React, { Component } from "react";
const Navbar = ({ counters }) => {
  console.log(`render navbar`);

  return (
    <nav className="navbar navbar-light bg-light">
      <a className="navbar-brand" href="#">
        Navbar
      </a>
      <div className="badge-secondary mr-auto pl-2 pr-2 rounded">
        {counters}
      </div>
    </nav>
  );
};

export default Navbar;
