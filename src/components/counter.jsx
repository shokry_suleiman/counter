import React, { Component } from "react";

class Counter extends Component {
  // state = {
  //   value: this.props.value,
  // };

  componentDidUpdate(prevProps, prevState) {
    console.log("prevProps", prevProps);
    console.log("prevState", prevState);
  }

  styles = {
    fontSize: 50,
    fontWeight: "bold",
  };
  //   constructor() {
  // super();
  // this.handleIncement = this.handleIncement.bind(this);
  //   }
  // handleIncement = (product) => {
  //   console.log("Shoukry Shoukry Suleiman ", product);
  //   this.setState({ value: ++this.state.value });
  // };

  //   doHandleIncrement = () => {
  //       this.handleIncement({id:1})
  //   };

  render() {
    console.log(`render counter`);

    console.log("props", this.props);
    return (
      <div>
        {this.props.children}
        <span className={this.counterBadgeClasses()}>{this.formatCount()}</span>
        <button
          onClick={() => this.props.onIncrement(this.props.counter)}
          className="btn btn-secondary btn-sm"
        >
          Increment
        </button>
        <button
          className="btn btn-danger btn-sm m-2"
          onClick={() => this.props.onDelete(this.props.id)}
        >
          Delete
        </button>
        {/* <ul>
          {this.state.tags.map((tag) => (
            <li key={tag}>{tag}</li>
          ))}
        </ul> */}
      </div>
    );
  }

  formatCount() {
    console.log("this.props value", this.props.value);
    // console.log("this.props counter", this.props.counter.valu);
    const { value } = this.props.counter;
    console.log("value", value);
    return value === 0 ? "Zero" : value;
  }

  counterBadgeClasses() {
    let classes = "badge  m-2 ";
    classes +=
      this.props.counter.value === 0 ? "badge-warning" : "badge-primary";
    return classes;
  }
}

export default Counter;
